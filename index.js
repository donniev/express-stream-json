'use strict';
let Stream = require("stream"),
	through2 = require("through2");
module.exports = (req, res, next)=> {
	if (!req.resultStream) {
		next();
		return;
	}
	if (!(req.resultStream instanceof Stream)) {
		next();
		return;
	}
	if (!req.resultStream.readable) {
		next();
		return;
	}
	let chunksSent = 0;
	req.resultStream.pipe(through2(function (chunk, enc, cb) {
		if (chunksSent === 0) {
			this.push(new Buffer("["));
		}
		if (chunksSent > 0) {
			this.push(new Buffer(","));
		}
		this.push(chunk);
		chunksSent++;
		cb();
	}, function () {
		if(chunksSent >0) {
			this.push(new Buffer("]"));
		}else{
			this.push(new Buffer("[]")); //means nothing sent
		}
		this.push(null);
	})).pipe(res);
}