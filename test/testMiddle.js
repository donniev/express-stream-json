/**
 * module testMiddle
 *
 */
'use strict';
let Stream = require("stream"),
	util = require("util");
let myStream;
describe("basic functionality", ()=> {
	it("test read stream", (done)=> {
		class Instream extends Stream.Readable {
			constructor() {
				super()
			}
			_read() {
				this.push(JSON.stringify({rowNumber: 1}))
				this.push(JSON.stringify({rowNumber: 2}))
				this.push(JSON.stringify({rowNumber: 3}))
				this.push(JSON.stringify({rowNumber: 4}))
				this.push(JSON.stringify({rowNumber: 5}))
				return this.push(null)
			}
		}
		let output="";
		class OutStream extends Stream.Writable {
			constructor(){
				super()
			}
			_write(chunk,enc,cb){
				console.log(chunk.toString());
				output+=chunk.toString();
				cb();
			}
		}
		let outStream=new OutStream();
		outStream.on("finish",()=>{
			console.log(output);
			let outObj=JSON.parse(output);
			expect(outObj.length).toBe(5);
			expect(outObj[1].rowNumber).toBe(2);
			done();
		})
		var inStream = new Instream();
		require("../index")({resultStream: inStream}, outStream, ()=>{console.log("next called");});
	})
})